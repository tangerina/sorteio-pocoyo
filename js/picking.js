function bestTeamSize(players) {
  let size = Infinity;
  let totalTeams = 0;
  do size = players.length / ++totalTeams;
  while (size > MAX_PER_TEAM);

  let result = {};
  result.totalPlayers = players.length;
  result.totalTeams = totalTeams;
  result.bestTeamSize = Math.floor(size);
  result.avgPlayers = size;

  return result;
}

function getTeams(players, teamEvaluation) {
  let teams = [];
  let team = {};
  do {
    team = pickTeam(players, teamEvaluation);
    teams.push(team);
  } while (players.length > 0);

  let calibratedTeams = calibrateTeams(teams);
  console.log("calibrateTeams");
  console.log(calibratedTeams);

  return calibratedTeams;
}

function pickTeam(players, teamEvaluation) {
  let teamSize = 0;
  let bestTeamSize = teamEvaluation.bestTeamSize;
  //   let z = new Ziggurat();
  //   let gaussian = z.nextGaussian();
  //   console.log(gaussian);
  teamSize = getRandomTeamSize(bestTeamSize);
  let team = {};
  let nomeTime =
    teamNames[getRandomIndex(teamNames)] +
    " " +
    teamSurnames[getRandomIndex(teamSurnames)] +
    " " +
    teamPlaces[getRandomIndex(teamPlaces)];
  team.name = nomeTime;
  team.players = [];

  do {
    let idx = getRandomIndex(players);
    let player = players.splice(idx, 1);
    if (team.players.length == 0) {
      player.role = "Capitão";
    }
    team.players.push(player);
    teamSize--;
    if (teamSize == 0 && player.role == undefined) {
      player.role = "Piloto";
    }
  } while (teamSize > 0 && players.length > 0);

  return team;
}

function calibrateTeams(teams) {
  console.log(teams);
  let maxInTeams = 0;
  let minInTeams = 999;
  teams.forEach((team) => {
    maxInTeams =
      maxInTeams < team.players.length ? team.players.length : maxInTeams;
    if (minInTeams > team.players.length) {
      minInTeams = team.players.length;
    }
  });

  if (minInTeams == maxInTeams) {
    return teams;
  }

  if (minInTeams == 1 && maxInTeams < MAX_PER_TEAM) {
    let playerToMove = null;
    for (let i = 0; i < teams.length; i++) {
      if (minInTeams == teams[i].players.length) {
        playerToMove = teams[i].players.splice(0, 1);
        teams.splice(i, 1);
      }
    }
    for (let i = 0; i < teams.length; i++) {
      if (MAX_PER_TEAM > teams[i].players.length && playerToMove != null) {
        playerToMove[0].role = null;
        teams[i].players.push(playerToMove[0]);
        playerToMove = null;
        break;
      }
    }
  }

  return teams;
}

document.addEventListener("DOMContentLoaded", function () {
  document.querySelector("[name=convidado]").value = "";
  document
    .querySelector("[name=convidado]")
    .addEventListener("keypress", function (e) {
      if (e.key === "Enter") {
        addConvidado(this);
      }
    });
  const divPocoyos = document.querySelector(".pocoyos");
  const divGuests = document.querySelector(".guests");
  for (var i = 0; i < pocoyos.length; i++) {
    divPocoyos.appendChild(montarCardPlayer(pocoyos[i]));
  }
  for (var i = 0; i < guests.length; i++) {
    divGuests.appendChild(montarCardPlayer(guests[i]));
  }
});

function addConvidado(evt) {
  let convidado = document.querySelector("[name=convidado]").value;
  document.querySelector("[name=convidado]").value = "";
  if (convidado == "") {
    exibeMensagem("Insira um nome para o convidado!");
    return;
  }
  if (findPlayer(convidado)) {
    exibeMensagem("O jogador " + convidado + " já está na lista!");
    return;
  }
  let player = {};
  player.name = convidado;
  player.gamertag = convidado;

  const divGuests = document.querySelector(".guests");
  divGuests.appendChild(montarCardPlayer(player));
}

function findPlayer(playerName) {
  let found = false;
  const pocoyos = document.querySelector(".pocoyos");
  const guests = document.querySelector(".guests");

  Array.from(pocoyos.children).forEach((el) => {
    let itGamertag = el.querySelector("label > .gamertag");
    found = found || playerName == itGamertag.textContent;
  });

  Array.from(guests.children).forEach((el) => {
    let itGamertag = el.querySelector("label > .gamertag");
    found = found || playerName == itGamertag.textContent;
  });
  return found;
}

function sortear() {
  let players = getArraySorteio();
  let teamEvaluation = bestTeamSize(players);
  let teams = getTeams(players, teamEvaluation);

  console.log(teams);
  preencherSorteio(teams);
}

function preencherSorteio(times) {
  times.forEach((time) => {
    createTeamCard(time);
  });
  swap();
}

function createTeamCard(time) {
  let el = document.createElement("div");
  el.classList.add("card-team");
  let title = document.createElement("div");
  title.classList.add("team-name");
  title.textContent = time.name;
  el.appendChild(title);
  let divPlayers = document.createElement("div");
  divPlayers.classList.add("team-players");
  time.players.forEach((player) => {
    let playerCard = document.createElement("div");
    let regiment = document.createElement("span");
    regiment.classList.add("regimenttag");
    regiment.textContent = player[0].regimentTag;
    let gamertag = document.createElement("span");
    gamertag.classList.add("gamertag");
    gamertag.textContent = player[0].gamerTag;
    let role = document.createElement("span");
    role.classList.add("role");
    role.textContent = player.role;
    playerCard.appendChild(regiment);
    playerCard.appendChild(gamertag);
    playerCard.appendChild(role);
    divPlayers.appendChild(playerCard);
  });
  el.appendChild(divPlayers);
  document.querySelector(".result-content").appendChild(el);
}

function swap() {
  document.querySelector(".content").classList.toggle("escondido");
  document.querySelector(".result").classList.toggle("escondido");
}

function reset() {
  document.querySelector(".result-content").innerHTML = "";
  swap();
}

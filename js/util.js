function exibeMensagem(text) {
  console.log(text);
}

function getRandomIndex(array) {
  return Math.floor(Math.random() * array.length);
}

function rolld10() {
  let roll = Math.floor(Math.random() * 10) + 1;
  return roll;
}

function getRandomTeamSize(best, min = 1, max = MAX_PER_TEAM) {
  let size = getGaussianTeamSize(best);
  if (size < min) size = min;
  if (size > max) size = max;
  return size;
}

function getGaussianTeamSize(bestTeamSize) {
  //rolar 10 d10
  let teamSize = 0;
  let diceRoll = 0;
  for (let i = 0; i < 10; i++) {
    diceRoll += rolld10();
  }

  if (diceRoll < 15) {
    teamSize = bestTeamSize - 2;
  }
  if (diceRoll >= 15 && diceRoll < 40) {
    teamSize = bestTeamSize - 1;
  }
  if (diceRoll >= 40 && diceRoll < 80) {
    teamSize = bestTeamSize;
  }
  if (diceRoll >= 80 && diceRoll < 95) {
    teamSize = bestTeamSize + 1;
  }
  if (diceRoll >= 95) {
    teamSize = bestTeamSize + 2;
  }

  return teamSize;
}

function selectAllPocoyos() {
  selectAllByClass("pocoyos");
}

function selectAllGuests() {
  selectAllByClass("guests");
}

function selectAllByClass(classe) {
  let selector = "." + classe + " > .card-pocoyo > label > .ipt-recruit";
  let ipts = document.querySelectorAll(selector);
  Array.from(ipts).forEach((ipt) => {
    ipt.checked = true;
  });
}

function montarCardPlayer(player) {
  let el = document.createElement("div");
  el.classList.add("card-pocoyo");
  el.innerHTML +=
    '<!-- <img class="avatar" src="' + player.profilePic + '" /> -->';
  let lbl = createCheckbox(player);
  el.appendChild(lbl);
  return el;
}

function createCheckbox(player) {
  let lbl = document.createElement("label");
  lbl.classList.add("container-check");
  let regimenttag = document.createElement("span");
  regimenttag.classList.add("regimenttag");
  let gamertag = document.createElement("span");
  gamertag.classList.add("gamertag");

  if (player.regimentTag) {
    tnRegiment = document.createTextNode("[" + player.regimentTag + "]");
    regimenttag.appendChild(tnRegiment);
  }

  tnGamerTag = document.createTextNode(player.gamertag);
  gamertag.appendChild(tnGamerTag);

  let input = createInput();

  let span = document.createElement("span");
  span.classList.add("checkmark");

  lbl.appendChild(regimenttag);
  lbl.appendChild(gamertag);
  lbl.appendChild(input);
  lbl.appendChild(span);

  lbl.title = player.name;
  return lbl;
}

function createInput() {
  let input = document.createElement("input");
  input.classList.add("ipt-recruit");
  input.type = "checkbox";
  input.id = "recruit";
  input.name = "recruit";

  return input;
}

function getArraySorteio() {
  let jogadores = [];
  const pocoyos = document.querySelector(".pocoyos");
  const guests = document.querySelector(".guests");

  Array.from(pocoyos.children).forEach((el) => {
    let jogador = {};
    let player = el.querySelector("label");
    if (player.querySelector(".ipt-recruit:checked")) {
      jogador.regimentTag = player.querySelector(".regimenttag").textContent;
      jogador.gamerTag = player.querySelector(".gamertag").textContent;
      jogadores.push(jogador);
    }
  });

  Array.from(guests.children).forEach((el) => {
    let jogador = {};
    let player = el.querySelector("label");
    if (player.querySelector(".ipt-recruit:checked")) {
      jogador.regimentTag = player.querySelector(".regimenttag").textContent;
      jogador.gamerTag = player.querySelector(".gamertag").textContent;
      jogadores.push(jogador);
    }
  });

  return jogadores;
}

/* <div class="card-pocoyo">
<!-- <img class="avatar" src="assets/img/logo-white.png" /> -->
<label class="container-check"
    ><span class="regimenttag">[Pcys]</span>
    <span class="gamertag">gamertag</span>
    <input
    class="ipt-recruit"
    type="checkbox"
    name="recruit"
    id="recruit"
    />
    <span class="checkmark"></span>
</label>
</div> */
